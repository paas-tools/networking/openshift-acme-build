# openshift-acme-build

This project builds our own image for the [openshift-acme](https://github.com/tnozicka/openshift-acme) project.

We build our own image in the CERN GitLab registry as a protection against possible hijacking of the upstream project, which is not backed by a large company or foundation.
This is a concern because of the high level of privileges granted to the openshift-acme controller, which needs to manipulate secrets.

It also allows us better control on updates (the [provided deployment](https://github.com/tnozicka/openshift-acme/tree/master/deploy/letsencrypt-live/cluster-wide) uses an auto-updating imageStream to redeploy on any update of the upstream image)

The resulting image is then deployed by the [dns-manager Helm chart](https://gitlab.cern.ch/paas-tools/infrastructure/dns-manager)

## Updating the image

Simply [update the `openshift-acme` git submodule](https://stackoverflow.com/a/5828396) to rebuild the upstream project from a newer git ref.
